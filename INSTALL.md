## Official packages

These packages are maintained by project members.

| Repository | Package |
| - | - |
| Flathub | [io.gitlab.zehkira.Monophony](https://flathub.org/apps/details/io.gitlab.zehkira.Monophony) |

## Community packages

These packages are maintained by third parties. Install at your own risk.

| Repository | Package |
| - | - |
| Arch User Repository | [monophony](https://aur.archlinux.org/packages/monophony)
| Snap | [monophony](https://snapcraft.io/monophony)

## Building from source

Follow these steps to build the app from source for testing purposes:

1. Install `git` and `flatpak-builder`.
2. Download the repository: `git clone https://gitlab.com/zehkira/monophony.git`
3. Enter the source directory: `cd monophony/source`
4. Build and install the app as a Flatpak: `make flatpak`
