Monophony is a free and open source Linux app for streaming music from YouTube. It has no ads and does not require an account.

You are expected to follow the [GNOME Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) when interacting with this repository.

Copyright © 2022-2023 zehkira, [AGPLv3](https://gitlab.com/zehkira/monophony/-/blob/master/source/LICENSE).

| [Install](https://gitlab.com/zehkira/monophony/-/blob/master/INSTALL.md) |
|-|

<img src='https://gitlab.com/zehkira/monophony/-/raw/master/assets/screenshot1.png' alt='screenshot'>
